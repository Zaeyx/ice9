#Spread the freeze

import sys
import random

arr = [
	["t","e","s","t"],
	["st","er","fg","tr"],
	["egg","boo","cup","toy"],
	["word","test","cake","mean"],
	["Fight","moves","twist","strip"],
	["strike","fullon","combat","attack"],
	["measure","carries","destroy","invalid"]
]

def isint(inp):
	vals = ["1","2","3","4","5","6","7","8","9","0"]
	for val in vals:
		if inp == val:
			return True
	return False

if len(sys.argv) < 3:
	print "Proper Usage: %s <input_file> <output_file>" % sys.argv[0]
	exit(0)

fi = open(sys.argv[1],"r")
data = fi.read()
fi.close()

out = ""
count = 0
for word in data.split("_Z"):
	if "v" in word and isint(word[0]):
		t =  word.split("v")
		print t[0]
		f = len(t[0]) - 2
		if f <= len(arr) - 1 and f > 0:
			rep = t[0][0] + arr[f][random.randint(0,len(arr[f])-1)]
			rep =  "_Z" + rep + "v" + "v".join(t[1:])
			print repr(rep), len(rep)
			print repr(word), len(word)
			print
			out = out + rep
		else:
			out = out + "_Z" + word
	else:
		if count != 0:
			out = out + "_Z" + word
		else:
			out = out + word
			count = count + 1

print len(data), len(out)

fi = open(sys.argv[2],"w")
fi.write(out)
fi.close()

for i in range(len(data)):
	print repr(data[i]) + "\t" + repr(out[i])
